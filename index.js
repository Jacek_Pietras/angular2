/* jshint node: true */
var express = require('express');
var moragn = require("morgan");
var app = express();
var httpServer = require("http").Server(app);
var path = __dirname + "/app/views/";

var port = process.env.PORT || 3000;

app.use(moragn("dev"));
app.use(express.static(__dirname));
app.use("/ang2", express.static("node_modules/angular2/bundles/"));
app.use("/sysjs", express.static("node_modules/systemjs/dist/"));
app.use("/rxjs", express.static("node_modules/rxjs/bundles/"));
app.use("/es6", express.static("node_modules/es6-shim/"));
app.use("/ts", express.static("node_modules/typescript/lib/"));



app.get("/", function (req, res) {
    res.sendFile(path + "index.html");
});

httpServer.listen(port, function () {
    console.log('Serwer HTTP działa na porcie ' + port);
});