import {Component} from 'angular2/core';


interface Hero {
    id: number;
    name: string;
}


@Component({
    selector: 'my-app',
    template: `
    <h1>{{title}}</h1>
    <h2>{{hero.name}} rozpoczęta!</h2>
    <div><label>id: </label>{{hero.id}}</div>
    <div>
    <label>Imię: </label>
    <div><input [(ngModel)]="hero.name" placeholder="Podaj imię"></div>
    </div>
    `
})

export class AppComponent {
    public title = "Wyprawa Bohatera";
    public hero: Hero = {
        id: 1,
        name: "Jaxilus"
    };
}
